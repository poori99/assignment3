#include <stdio.h>

int main()
{
    char ch;
    printf("Enter Any Lowercase or Uppercase Character: ");
    scanf("%c",&ch);

    if(ch>=65 && ch<=90)
    {
        printf("%c is an Uppercase Character.",ch);
    }
    else
    {
        printf("%c is a Lowercase Character.\n",ch);
    }
    return 0;
}
