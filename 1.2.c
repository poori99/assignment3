#include <stdio.h>

int main()
{
    int n;
    printf("Enter any year to check leap or not: ");
    scanf("%d",&n);

    if(n%4 == 0)
    {
        if(n%100 == 0)
        {
            if(n%400 == 0)
                printf("%d is a Leap Year.\n",n);
            else
                printf("%d is not a Leap Year.\n",n);
        }
        else
            printf("%d is a Leap Year.\n",n);
    }
    else
        printf("%d is not a Leap Year.\n",n);

    return 0;
}
