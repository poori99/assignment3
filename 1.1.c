//c program to check whether a number is positive, negative or zero
#include <stdio.h>

int main()
{
    int n;
    printf("Enter any number: ");
    scanf("%d",&n);

    if(n<0)
    {
        printf("%d is a negative number\n",n);
    }
    else if(n>0)
    {
        printf("%d is a positive number\n",n);
    }
    else
    {
        printf("%d is zero\n",n);
    }

    return 0;
}
